var kpiGraph;
function graph(graphContainer) {
    var g = {
        nodes: [],
        edges: []
    };
    kpiGraph = this;
    //this.linethin = 0.5;
    var lt = 0.5;
    this.linechange = function () {
        lt = parseFloat(this.scope.linet);
        console.log(lt + 10);
    }
    var s = this.s = new sigma(
        {
            graph: g,
            renderer: {
                container: document.getElementById(graphContainer),
                type: 'canvas'
            },
            settings: {
                minEdgeSize: 1,
                maxEdgeSize: 10,

                minNodeSize: 6,
                maxNodeSize: 20,
                dragNodeStickiness: 0.01,
                borderSize: 3,
                outerBorderSize: 5,
                defaultNodeBorderColor: '#FFF',
                defaultNodeOuterBorderColor: 'rgb(236, 81, 72)',
                enableEdgeHovering: true,
                edgeHoverHighlightNodes: 'circle',
            }
        }
    );


    s.refresh();
    var mode = "move";
    var ecolor = "red";
    this.setcolor = function (cl) {
        mode = "add";
        ecolor = cl;
        this.s.refresh();
    };
    this.move = function () {
        mode = "move";
        this.s.refresh();
    };
    this.del = function () {
        nodestore[lastNode.id].icon = "";
        s.graph.dropNode(lastNode.id);
        mode = "del";
        this.s.refresh();
    }

    var nodestore = [];

    this.addNode = function (o) {
        nodestore[o.id] = o;
        try {
            s.graph.addNode(
                {
                    label: o.name,
                    id: o.id,
                    x: 0.05 * o.id,
                    y: 0.05 * o.id,
                    size: 1,
                    color: '#ff0000'
                }
            )
        } catch (e) {
            s.graph.dropNode(o.id);
            nodestore[o.id].icon = "";
        }
        s.refresh();
    }


    var activeState = sigma.plugins.activeState(this.s);

    var dragListener = sigma.plugins.dragNodes(this.s, this.s.renderers[0], activeState);
// Initialize the Select plugin:
    var select = sigma.plugins.select(this.s, activeState);
    select.init();
    var keyboard = sigma.plugins.keyboard(s, s.renderers[0]);
    select.bindKeyboard(keyboard);


    dragListener.bind('startdrag', function (event) {
        console.log(event);
    });
    dragListener.bind('drag', function (event) {
        console.log(event);
    });
    dragListener.bind('drop', function (event) {
        console.log(event);
    });
    var lastNode;
    dragListener.bind('dragend', function (event) {
        //var xx = activeState;
        console.log(event.data.node.id, select);
        if (lastNode && mode == "add") {
            s.graph.addEdge({
                id: 'e0' + Math.random() * 1,
                source: lastNode.id,
                label: '%',
                target: event.data.node.id,
                color: ecolor,
                type: 'arrow', //['line', 'curve', 'arrow', 'curvedArrow'][Math.random() * 4 | 0],
                size: parseFloat(lt)
            });
        }
        lastNode = event.data.node;
        s.refresh();
    });

}


