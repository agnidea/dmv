var dmvApp = angular.module('dmvApp', ['ngRoute']);

dmvApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            controller: 'MainCtrl',
            templateUrl: appHost + '-/view/main.html'
        })
        .otherwise({
            redirectTo: '/home'
        });
});


var gs;
dmvApp.controller('MainCtrl', function ($scope) {
    $('.visible.example .ui.sidebar')
        .sidebar({
            context: '.visible.example .bottom.segment'
        })
        .sidebar('hide')
    ;
    $scope.template = {
        "kpiList": "-/view/kpi-list.html",
        "visGraph": "-/view/visual-graph.html",
        "toolPanel": "-/view/tools-panel.html"
    }

    $scope.message = {
        "values-kpi": "Message from home template",
        "visual-graph": "Message from about template",
        "tools-panel": "Message from contact template"
    }

    gs = $scope;
    $scope.n0 = "0111"
});

dmvApp.controller('GraphCtrl', function ($scope) {
    console.log("GraphCtrl");
    var gfx = new graph('GraphContainer');
    $scope.gfx = gfx;
    $scope.linet = "0.5";
    gfx.scope = $scope;
});

dmvApp.controller('KpiCtrl', function ($scope) {
    console.log("KpiCtrl");
    var k = new kpidata();
    $scope.list = k.data;
    $scope.k = k;
});
