function kpidata() {
    this.data = [
        {icon: "", id:"1", name: "Финансирование"},
        {icon: "", id:"2", name: "ВУЗ"},
        {icon: "", id:"3", name: "Отношение поступивших к закончивших институт"},
        {icon: "", id:"4", name: "Высокотехнологическое производство"},
        {icon: "", id:"5", name: "Кол-во научных статей в год"},
        {icon: "", id:"6", name: "Количество иностранных преподавателей в ВУЗах"},
        {icon: "", id:"7", name: "НИИ и КБ"},
        {icon: "", id:"8", name: "Эффективные вузы"},
        {icon: "", id:"9", name: "Неэффективные вузы"}
    ];

    this.select = function(o){
        //console.log(o);
        o.icon = "checkmark";
        kpiGraph.addNode(o);
    }
}

